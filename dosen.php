<?php
$host       = "localhost";
$user       = "root";
$pass       = "";
$db         = "penjadwalan_dosen";

$koneksi    = mysqli_connect($host, $user, $pass, $db);
if (!$koneksi) { //cek koneksi
    die("Tidak bisa terkoneksi ke database");
}
$foto_dosen        = "";
$nip_dosen       = "";
$nama_dosen     = "";
$prodi     = "";
$fakultas   = "";
$sukses     = "";
$error      = "";



if (isset($_GET['op'])) {
    $op = $_GET['op'];
} else {
    $op = "";
}
if($op == 'delete'){
    $id_dosen         = $_GET['id_dosen'];
    $sql1       = "delete from dosen where id = '$id_dosen'";
    $q1         = mysqli_query($koneksi,$sql1);
    if($q1){
        $sukses = "Berhasil hapus data";
    }else{
        $error  = "Gagal melakukan delete data";
    }
}
if ($op == 'edit') {
    $id        = $_GET['id_dosen'];
    $sql1       = "select * from dosen where id_dosen = '$id'";
    $q1         = mysqli_query($koneksi, $sql1);
    $r1         = mysqli_fetch_array($q1);
    $foto_dosen        = $r1['foto_dosen'];
    $nip_dosen       = $r1['nip_dosen'];
    $nama_dosen     = $r1['nama_dosen'];
    $prodi   = $r1['prodi'];
    $fakultas   = $r1['fakultas'];

    if ($nip_dosen == '') {
        $error = "Data tidak ditemukan";
    }
}
if (isset($_POST['simpan'])) { //untuk create
    $foto_dosen        = $_POST['foto_dosen'];
    $nip_dosen       = $_POST['nip_dosen'];
    $nama_dosen     = $_POST['nama_dosen'];
    $prodi     = $_POST['prodi'];
    $fakultas   = $_POST['fakultas'];

    if ($foto_dosen && $nip_dosen && $nama_dosen && $prodi && $fakultas) {
        if ($op == 'edit') { //untuk update
            $sql1       = "update dosen set foto_dosen='$foto_dosen'nip_dosen = '$nip_dosen',nama_doesn='$nama_dosen',prodi = '$prodi',fakultas='$fakultas' where id_dosen = '$id_dosen'";
            $q1         = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses = "Data berhasil diupdate";
            } else {
                $error  = "Data gagal diupdate";
            }
        } else { //untuk insert
            $sql1   = "insert into dosen(foto_dosen,nip_dosen,nama_dosen,prodi,fakultas) values ('$foto_dosen','$nip_dosen','$nama_dosen','$prodi','$fakultas')";
            $q1     = mysqli_query($koneksi, $sql1);
            if ($q1) {
                $sukses     = "Berhasil memasukkan data baru";
            } else {
                $error      = "Gagal memasukkan data";
            }
        }
    } else {
        $error = "Silakan masukkan semua data";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <style>
        .mx-auto {
            width: 800px
        }

        .card {
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <div class="mx-auto">
        <!-- untuk memasukkan data -->
        <div class="card">
            <div class="card-header">
                Create / Edit Data
            </div>
            <div class="card-body">
                <?php
                if ($error) {
                ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error ?>
                    </div>
                <?php
                    header("refresh:10;url=dosen.php");//5 : detik
                }
                ?>
                <?php
                if ($sukses) {
                ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $sukses ?>
                    </div>
                <?php
                    header("refresh:10;url=dosen.php");
                }
                ?>
                <form action="" method="POST">
                    <div class="mb-3 row">
                        <label for="foto_dosen" class="col-sm-2 col-form-label">FOTO</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="foto_dosen" name="foto_dosen" value="<?php echo $foto_dosen ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="nip_dosen" class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="nip_dosen" name="nip_dosen" value="<?php echo $nip_dosen ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="nama_dosen" class="col-sm-2 col-form-label">NAMA</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_dosen" name="nama_dosen" value="<?php echo $nama_dosen ?>">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="prodi" class="col-sm-2 col-form-label">PRODI</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="prodi" name="prodi" value="<?php echo $prodi ?>">
                               
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="fakultas" class="col-sm-2 col-form-label">FAKULTAS</label>
                        <div class="col-sm-10">
                            
                            <input type="text" class="form-control" id="fakultas" name="fakultas" value="<?php echo $fakultas ?>">
            
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>

        <!-- untuk mengeluarkan data -->
        <div class="card">
            <div class="card-header text-white bg-secondary">
                Data Dosen
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">FOTO</th>
                            <th scope="col">NIP</th>
                            <th scope="col">NAMA DOSEN</th>
                            <th scope="col">PRODI</th>
                            <th scope="col">FAKULTAS</th>
                            <th scope="col">KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sql2   = "select * from dosen order by id_dosen desc";
                        $q2     = mysqli_query($koneksi, $sql2);
                        $urut   = 1;
                        while ($r2 = mysqli_fetch_array($q2)) {
                            $foto_dosen         = $r2['foto_dosen'];
                            $nip_dosen        = $r2['nip_dosen'];
                            $nama_dosen        = $r2['nama_dosen'];
                            $prodi       = $r2['prodi'];
                            $fakultas   = $r2['fakultas'];

                        ?>
                            <tr>
                                <th scope="row"><?php echo $urut++ ?></th>
                                <td scope="row"><?php echo $foto_dosen ?></td>
                                <td scope="row"><?php echo $nip_dosen ?></td>
                                <td scope="row"><?php echo $nama_dosen ?></td>
                                td scope="row"><?php echo $prodi ?></td>
                                <td scope="row"><?php echo $fakultas ?></td>
                                <td scope="row">
                                    <a href="dosen.php?op=edit&id=<?php echo $id_dosen ?>"><button type="button" class="btn btn-warning">Edit</button></a>
                                    <a href="dosen.php?op=delete&id=<?php echo $id_dosen?>" onclick="return confirm('Yakin mau delete data?')"><button type="button" class="btn btn-danger">Delete</button></a>            
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                     
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
